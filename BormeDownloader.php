<?php
require 'pdfparser/vendor/autoload.php';

class BormeDownloader
{
    // Constantes que definen los directorios de trabajo
    static $BORME_DIR_PDF = 'borme_pdf';
    static $BORME_DIR_TXT = 'borme_txt';

    /**
     * Version multiple del metodo downloadBorme. Trabaja con peticiones asíncronas vía cUrl 
	 * para descargar varios ficheros PDF de forma paralela, mejorando el rendimiento del
	 * método secuencial.
	 * 
     * @param array<string> $urls: conjunto de urls a ficheros BORME
     * @return array<string>: conjunto de rutas a ficheros de texto que contienen el contenido de los BORME
     */
    public static function downloadBormeMultiple(array $urls) {
        // Controlamos la existencia de los directorios necesarios
        self::checkDirs();
		
		// Usaremos un conjunto de peticiones cUrl asincronas
        $requests = array();
        $pdfFiles = $txtFiles = array();
        $multipleCurl = curl_multi_init();

        // Generamos una peticion por cada url en el parametro de entrada al metodo
        foreach($urls as $url){
			if(self::validateUrl($url)){
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
				$requests[] = $curl;
				curl_multi_add_handle($multipleCurl, $curl);
			}else{
				$requests[] = null;
			}
        }

        // Lanzamos las peticiones
        do {
            curl_multi_exec($multipleCurl, $active);
        } while ($active > 0);

        // Recorremos las peticiones asincronas realizadas
        foreach($requests as $i => $req){
			if(!is_null($req)){
				// Generamos la ruta para descargar el fichero
				$pdfPath = self::generatePdfPath($urls[$i]);
		
				// Obtenemos la respuesta de la petición cUrl
				$curlError = curl_error($req);
				if (empty($curlError)) {
					$responseCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
					$response = curl_multi_getcontent($req);
					// Almacenamos el fichero PDF
					if($responseCode == 200 
						&& !empty($response) 
						&& file_put_contents($pdfPath, $response) != FALSE){
						$pdfFiles[$i] = $pdfPath;
					}
				}
				
				// Cerramos la petición
				curl_multi_remove_handle($multipleCurl, $req);
				curl_close($req);
			}
			
			// Validamos que se haya descargado un fichero PDF
            if(empty($pdfFiles[$i]) || !file_exists($pdfPath)){
				$pdfFiles[$i] = null;
            }
        }
        curl_multi_close($multipleCurl);

		// Recorremos los ficheros PDF que han podido ser descargados
		foreach($pdfFiles as $i => $pdfFile){
			if(!is_null($pdfFile)){
				// Extraemos el contenido del fichero PDF
				$pdfText = self::readPdfContent($pdfFile);
				if(!empty($pdfText)){
					// Almacenamos el contenido en un fichero de texto
					$txtPath = str_replace('pdf', 'txt', $pdfFile);
					if(file_put_contents($txtPath, $pdfText) != FALSE){
						$txtFiles[$i] = $txtPath;
						// Eliminamos el fichero PDF
						unlink($pdfFile);
					}
				}
				if(empty($txtFiles[$i])){
					$txtFiles[$i] = null;
				}
			}
		}
		
        // Devolvemos el array de ficheros txt
        return $txtFiles;
    }

    /**
     * Metodo que dada una url a un fichero PDF del BORME, lo
     * descarga, extrae su texto y lo almacena en otro fichero.
     *
     * @param string $url: url del PDF del BORME a utilizar
     * @return string: enlace al fichero contenedor del texto del fichero PDF
     *
     * @throws Exception cuando ocurra algun error durante el proceso
     */
    public static function downloadBorme($url) {
        // Controlamos la existencia de los directorios necesarios
        self::checkDirs();

		// Validamos la url al fichero PDF
		if(!self::validateUrl($url)){
			throw new Exception("invalid URL {$url}");
		}
		
        // Generamos una ruta y descargamos el fichero
        $pdfPath = self::generatePdfPath($url);
        self::downloadFile($url, $pdfPath);

        // Extremos el texto del fichero PDF
		$pdfText = self::readPdfContent($pdfPath);
		if(empty($pdfText)){
            throw new Exception("pdf file {$pdfPath} could not be read");
        }

        // Almacenamos el texto extraido del PDF en un fichero txt
        $txtPath = str_replace('pdf', 'txt', $pdfPath);
        if(file_put_contents($txtPath, $pdfText) == FALSE){
            throw new Exception("txt file {$pdfPath} could not be written");
        }

        // Eliminamos el PDF original
        unlink($pdfPath);

        // Retornamos la ruta al fichero de texto
        return $txtPath;
    }
	
	/** 
	* Método que dada una ruta local a un fichero PDF, extrae y devuelve su contenido textual.
	*
	* @param string $pdfPath: ruta al fichero PDF
	* @return string: contenido textual extraido del fichero PDF. Cadena vacía en caso de error.
	*/
	private static function readPdfContent($pdfPath){
		try{
			$pdfParser = new \Smalot\PdfParser\Parser();
			$pdfText = $pdfParser->parseFile($pdfPath)->getText();
			return $pdfText;
		}catch(Exception $ex){
			return '';
		}
	}

	/**
	* Método que dada una url de un fichero PDF del BORME, devuelve la ruta que
	* debería contenerlo a nivel local.
	*
	* Se extrae el nombre del ficheo de la url y se le concatena un hash para
	* evitar conflictos por nombre.
	*
	* @param string $url: url al fichero PDF del BORME
	* @return string: ruta a la localización local donde almacenarlo
	*/
	private static function generatePdfPath($url) {
		$pdfName = strtolower(substr($url, strrpos($url, '/')+1));
        $ds = self::getDirectorySeparator();
        $pdfPath = str_replace('.pdf', '_'.uniqid().'.pdf', $pdfName);
        $pdfPath = self::$BORME_DIR_PDF . $ds . $pdfPath;
		return $pdfPath;
	}
	
    /**
     * Método que descarga y almacena un fichero de $url y lo almacena en $path.
     * Se realizan hasta 3 intentos de descarga si hay algún problema, esperando
     * 10 segundos entre cada uno de ellos.
     *
     * @param string $url: Url que apunta a un fichero almacenado en un servidor externo.
     * @param string $path: Ruta local donde almacenar el fichero.
     *
     * @throws Exception cuando el fichero no haya podido ser descargado.
     */
    private static function downloadFile($url, $path) {
        $request = 3;
        do{
            // Intento de descarga del fichero vía cUrl
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            $file = curl_exec ($curl);
            $error = curl_error($curl);
			$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close ($curl);
            --$request;

			// Validamos que no haya errores
			if($code != 200 || !empty($error)){
				$file = null;
			}
			
            // Esperamos 10 segundos si ha ocurrido algun error
            if(empty($file) && $request > 0){
                sleep(10);
            }
        }while(empty($file) && $request > 0);

        // Lanzamos una excepción si el fichero no ha podido ser descargado
        if(empty($file)){
            throw new Exception("file from '{$url}' could not be downloaded");
        }

        // Almacenamos el fichero en la ruta local
        file_put_contents($path, $file);
    }

    /**
     * Método que devuelve el separador de directorios
     * según el sistema operativo que ejecute el script.
     *
     * @return string: separador de directorios correcto
     */
    private static function getDirectorySeparator(){
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            // Windows
            return '\\';
        }
        // Otro SO
        return '/';
    }

    /**
     * Método que comprueba que existan los directorios
     * necesarios para analizar los PDFs de BORME.
     * Se crearán en caso de no existir.
     */
    private static function checkDirs() {
        if(!is_dir(self::$BORME_DIR_PDF)){
            mkdir(self::$BORME_DIR_PDF);
        }
        if(!is_dir(self::$BORME_DIR_TXT)){
            mkdir(self::$BORME_DIR_TXT);
        }
    }
	
	/**
	* Método que dada una url valida que sea correcta. Comprueba que la url esté bien
	* construida, el host sea la web del Boletín Oficial del Estado y el recurso sea
	* un fichero con extensión PDF.
	*
	* @param string $url: url a validar.
	* @return boolean: TRUE si es correcta. FALSE en caso contrario.
	*/
	private static function validateUrl($url){
		// Utilizamos el filtro para URLs de PHP: FILTER_VALIDATE_URL
		if (filter_var($url, FILTER_VALIDATE_URL) !== FALSE) {
			$urlParts = parse_url($url);
			// Validamos el host (www.boe.es) y que el recurso sea un fichero PDF
			if(strcmp($urlParts['host'], 'www.boe.es') == 0
				&& strcmp(strtolower(substr($urlParts['path'], -4)), '.pdf') == 0){
				return TRUE;
			}
		}
		return FALSE;
	}
}