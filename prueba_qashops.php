<?php
	require 'BormeDownloader.php';
		
	BormeDownloader::downloadBormeMultiple([
		'https://www.boe.es/borme/dias/2017/01/10/pdfs/BORME-A-2017-6-41.pdf',
		'https://www.boe.es/borme/dias/2018/04/30/pdfs/BORME-A-2018-83-11.pdf',
		'https://www.boe.es/borme/dias/2018/04/30/pdfs/BORME-A-2018-83-43.pdf',
		'https://www.urlinexistente.com/archivo.pdf', // Host inválido, no debe descargar nada
		'https://www.boe.es/borme/dias/2018/04/30/pdfs/BORME-A-2018-83-99.pdf',
		'https://www.boe.es/borme/dias/2018/04/30/pdfs/BORME-A-2018-83-12.pdf',
		'https://www.boe.es/borme/dias/2018/04/30/pdfs/BORME-A-2018-99-99.pdf' // No existe, no debe descargar nada
	]);
?>
