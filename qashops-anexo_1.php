<?php
require_once 'OrderLine.php';
require_once 'BlockedStock.php';
require_once 'ShoppingCart.php';
require_once 'ShopChannel.php';

class Product
{
    /**
     * Metodo que devuelve el stock real de un producto dado, teniendo en cuenta
     * las unidades que pueden estar bloqueadas por pedidos en curso.
     *
     * @param int $productId: identificador unico del producto
     * @param int $quantityAvailable: cantidad disponible sin tener en cuenta las unidades bloqueadas
     * @param bool $cache: TRUE cuando haremos uso de la cache. FALSE por defecto.
     * @param int $cacheDuration: tiempo de cache en segundos. 60 segundos por defecto en caso de usar cache.
     * @param object $securityStockConfig: configuracion de seguridad
     * @return int: stock real del producto
     */
    public static function stock(
        $productId,
        $quantityAvailable,
        $cache = false,
        $cacheDuration = 60,
        $securityStockConfig = null
    ) {
        // Si la cantidad disponible es negativa, la retornamos
        if($quantityAvailable < 0){
            return $quantityAvailable;
        }

        // Control de cache y tiempo de cache
        $cacheDuration = $cache ? $cacheDuration : 0;

        // Obtenemos el stock bloqueado por pedidos en curso
        $ordersQuantity = self::getOrdersQuantity($productId, $cacheDuration);

        // Obtenemos el stock bloqueado
        $blockedStockQuantity = self::getBlockedStockQuantity($productId, $cacheDuration);

        // Calculamos las unidades disponibles
        $quantity = $quantityAvailable - $ordersQuantity - $blockedStockQuantity;
        if (!is_null($securityStockConfig)) {
            $quantity = ShopChannel::applySecurityStockConfig(
                $quantity,
                $securityStockConfig->mode,
                $securityStockConfig->quantity
            );
        }

        // Retornamos el stock, controlando que sea >= 0
        return $quantity > 0 ? $quantity : 0;
    }

    /**
     * Metodo que obteiene el stock bloqueado por pedidos en curso.
     *
     * @param int $productId: identificador unico del producto.
     * @param int $cacheDuration: tiempo de duracion de la cache. 0 para no cachear.
     * @return int: sumatorio de cantidad del producto $productId en pedidos pendientes
     */
    private static function getOrdersQuantity($productId, $cacheDuration = 0){
        // Listado de estados de pedido que corresponden a PENDIENTE
        $statesPendding = implode(',', array(Order::STATUS_PENDING, Order::STATUS_PROCESSING, Order::STATUS_WAITING_ACCEPTANCE));

        if ($cacheDuration > 0) {
            // Obtenemos la cantidad haciendo uso de la cache (llamada recursiva)
            $ordersQuantity = OrderLine::getDb()->cache(function ($db) use ($productId) {
                return self::getOrdersQuantity($productId);
            }, $cacheDuration);
        }else{
            // Obtenemos la cantidad sin hacer uso de la cache
            $ordersQuantity = OrderLine::find()
                ->select('SUM(quantity) as quantity')
                ->joinWith('order')
                ->where("order_line.product_id = {$productId} AND order.status IN ({$statesPendding})")
                ->scalar();
        }

        return !empty($ordersQuantity) ? intval($ordersQuantity) : 0;
    }

    /**
     * Metodo que obteiene el stock bloqueado en carritos de la compra.
     *
     * @param int $productId: identificador unico del producto.
     * @param int $cacheDuration: tiempo de duracion de la cache. 0 para no cachear.
     * @return int: sumatorio de cantidad del producto $productId bloqueado en carritos.
     */
    private static function getBlockedStockQuantity($productId, $cacheDuration = 0){
        // Estado de carrito PENDIENTE
        $statePendding = ShoppingCart::STATUS_PENDING;

        if ($cacheDuration > 0) {
            // Obtenemos la cantidad haciendo uso de la cache (llamada recursiva)
            $blockedStockQuantity = BlockedStock::getDb()->cache(function ($db) use ($productId) {
                return self::getBlockedStockQuantity($productId);
            }, $cacheDuration);
        }else{
            // Obtenemos la cantidad sin hacer uso de la cache
            $blockedStockQuantity = BlockedStock::find()
                ->select('SUM(quantity) as quantity')
                ->joinWith('shoppingCart')
                ->where("
                    blocked_stock.product_id = {$productId} 
                    AND (
                        shopping_cart_id IS NULL 
                        OR shopping_cart.status = '{$statePendding}'
                    )
                    AND blocked_stock_to_date > NOW()
                ")
                ->scalar();
        }

        return !empty($blockedStockQuantity) ? intval($blockedStockQuantity) : 0;
    }
}

